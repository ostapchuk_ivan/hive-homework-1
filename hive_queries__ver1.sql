-- create database `test_shema`
CREATE DATABASE IF NOT EXISTS test_schema;
-- create database `test_shema2` with explicit location
CREATE DATABASE IF NOT EXISTS test_schema2 LOCATION '/';
-- list all databases
SHOW DATABASES;
-- list all databases which name has '2' in its name
SHOW DATABASES LIKE '*2*';
-- drop database `test_shema2`
DROP DATABASE test_schema2;


-- (everything below do in scope of `test_shema` database)
USE test_schema;
-- create managed table `tab1e_1` with columns (`id` - type integer, `name` - type string)
CREATE TABLE IF NOT EXISTS tab1e_1 (id int, name string);
-- insert (1, 'aaa'), (2, 'bbb') into table `tab1e_1`
INSERT INTO TABLE tab1e_1 VALUES (1, 'aaa'), (2, 'bbb');
-- show content of the table `tab1e_1`
SELECT * FROM tab1e_1;

-- add column (`type` - type int) into table `tab1e_1` after column `id` and before column `name`
ALTER TABLE tab1e_1 ADD COLUMNS (type int);
CREATE TABLE temp AS SELECT id, type, name FROM tab1e_1;
ALTER TABLE tab1e_1 RENAME TO drop_me;
ALTER TABLE temp RENAME TO tab1e_1;
DROP TABLE drop_me;
-- show content of the table `tab1e_1`
SELECT * FROM tab1e_1;

-- append table `tab1e_1` with (3, 2, 'ccc')
INSERT INTO TABLE tab1e_1 VALUES (3, 2, 'ccc');
-- show content of the table `tab1e_1`
SELECT * FROM tab1e_1;

-- convert this table `tab1e_1` into bucket table with bucket column `type`
ALTER TABLE tab1e_1 RENAME TO dropme;
CREATE TABLE table_1(id int, type int, name string) CLUSTERED BY(type) SORTED BY(id) INTO 5 BUCKETS;
FROM dropme INSERT OVERWRITE TABLE table_1 SELECT id, type, name;
DROP TABLE dropme;
-- show content of the table `tab1e_1`
SELECT * FROM table_1;

-- via hdfs show the list of all files which are used by `tab1e_1`
dfs -ls -R /warehouse/tablespace/managed/hive/test_schema.db/table_1 ;
-- [?in several steps?] make table `tab1e_1` consists of 1 file (in hdfs)
-- change table `tab1e_1` into external
-- in hdp 3 managed tables can ONLY be transactionable
-- so in in versions before hdp 3 command would be ALTER TABLE <table> SET TBLPROPERTIES('EXTERNAL'='TRUE')
-- but in hdp 3 in doesn`t change anything so i will just create the same table but external
ALTER TABLE table_1 RENAME TO dropme;
CREATE EXTERNAL TABLE table_1 AS SELECT id, type, name FROM dropme SORT BY id;
DROP TABLE dropme;
-- show table schema of `tab1e_1` to be sure table is external
DESC FORMATTED table_1;
-- drop table `tab1e_1`
DROP TABLE table_1;

-- create new unmanaged table `tab1e_2` (w/o bucketing) with the content of the previous table `tab1e_1`
-- path is hdfs://hdp-1.us-central1-a.c.galvanic-flame-250309.internal:8020/warehouse/tablespace/external/hive/test_schema.db/table_1 
CREATE EXTERNAL TABLE table_2 (id int, type int, name string);
LOAD DATA INPATH 'hdfs://hdp-1.us-central1-a.c.galvanic-flame-250309.internal:8020/warehouse/tablespace/external/hive/test_schema.db/table_1/' INTO TABLE table_2;
-- show content of the table `tab1e_2`
SELECT * FROM table_2;

-- [?in several steps?] change this table into unmanaged partitioned by `type` column
CREATE EXTERNAL TABLE new_table_1 (id int, name string) PARTITIONED BY(type int);
INSERT OVERWRITE TABLE new_table_1 PARTITION(type) SELECT id, name, type FROM table_2;
ALTER TABLE table_2 RENAME TO drop_me;
ALTER TABLE new_table_1 RENAME TO table_2;
DROP TABLE drop_me;
-- add new column (`action_time` - type date) at the latest column
ALTER TABLE table_2 ADD COLUMNS (action_time date);
CREATE EXTERNAL TABLE new_table_3 (id int, name string, action_time date) PARTITIONED BY (type int);
INSERT OVERWRITE TABLE new_table_3 PARTITION(type) SELECT id, name, action_time, type FROM table_2;
DROP TABLE table_2;
ALTER TABLE new_table_3 RENAME TO table_2;
-- show content of the table `tab1e_2`
SELECT * FROM table_2;

-- append table `tab1e_2` with (4, 'ddd', '2018-10-31', 2), (5, 'eee', '2019-11-01', 3)
INSERT INTO TABLE table_2 VALUES 
(4, 'ddd', '2018-10-31', 2), 
(5, 'eee', '2019-11-01', 3);
-- show content of the table `tab1e_2`
SELECT * FROM table_2;

-- show data ordered by `action_time`
SELECT * FROM table_2 ORDER BY action_time;

-- show data which `action_time` is greater than today (today is not hardcoded)
SELECT * FROM table_2 WHERE action_time > current_date;

-- show data which `action_time` is in 2019 year
SELECT * FROM table_2 WHERE action_time >= '2019-01-01';

-- for every `type` show first record only (orderied by `name` column)
SELECT min(name), type FROM table_2 GROUP BY type;

-- do previous step using OVER clause
SELECT DISTINCT type, min(name) OVER(PARTITION BY type ORDER BY type) FROM table_2;

-- list all partitions of `tab1e_2`
SHOW PARTITIONS table_2;
-- drop all partitions except partition '2' ('type=2')
ALTER TABLE table_2 DROP IF EXISTS PARTITION(type = '__HIVE_DEFAULT_PARTITION__');
ALTER TABLE table_2 DROP IF EXISTS PARTITION(type != '2');

-- show content of the table `tab1e_2`
SELECT * FROM table_2;

-- create new managed table `tab1e_3` from the select of `table_2`
--        with specific location
--        with compression snappy
--        with type orc
CREATE TABLE IF NOT EXISTS table_3 STORED AS ORC LOCATION '/hive/table_3' TBLPROPERTIES('ORC.COMPRESS'='SNAPPY') AS SELECT * FROM table_2;
-- show content of table `tab1e_3`
SELECT * FROM table_3;

-- list all tables in database `test_shema`
SHOW TABLES;
-- list all tables in database `test_shema` which name contains '3'
SHOW TABLES LIKE '*3*';
-- drop test_shema (in 1 command)
DROP DATABASE IF EXISTS test_schema CASCADE;